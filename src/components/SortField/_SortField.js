export default {
    name: 'SortField',
    props: {
        values: {
            type: Array
        },
        currentSort: {
            type: Object
        }
    },
    methods: {
        setSort(el){
            this.$emit('onChangeSort', el);
        }
    }
}