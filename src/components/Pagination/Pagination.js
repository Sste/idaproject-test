export default {
    props: {
        current: {
            type: String
        },
        allPages: {
            type: String
        },
        disable: {
            type: Object,
            default: () => {
                return {
                    prev: true,
                    next: true
                }
            }
        }
    },
    methods: {
        onPrev(){
            this.$emit('onPrev');
        },
        onNext(){
            this.$emit('onNext');
        },
    }
}