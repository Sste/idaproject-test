export default {
    name: 'SelectField',
    props: {
        data: {
            type: Array,
            default: () => []
        },
        isMultiple: {
            type: Boolean,
            default: false
        },
        valueName: {
            type: String,
            default: 'columns'
        }
    },
    data(){
        return {
            isShownDrop: false,
            default: {
                name: 'Выберите значение',
                value: 0
            },
            multipleValue: {
                name: '',
                value: 0
            },
            current: null,
            selected: [],
            values: this.data
        }
    },
    computed: {
    },
    methods: {
        setFirstValue() {
            if (!this.isMultiple) {
                this.current = (this.data && this.data.length) ? this.data[0] : this.default;
            } else {
                this.values.forEach(el => {
                    if (el.selected){
                        this.selected.push(el);
                    }
                });
                this.createMultipleValue();
            }
        },
        setValue(value){
            if (!this.isMultiple){
                this.current = value;
                this.selected = [value];
                this.isShownDrop = false;

                this.$emit('onChange', value);
            } else {
                this.createMultipleValue();

                value.selected = !value.selected;

                let vals = this.values;

                this.selected = [];

                vals.filter((el, i) => {
                    if (el.selected) this.selected.push(el);
                });

                this.$emit('onChange', value);
            }
        },
        createMultipleValue(){
            this.multipleValue.name = `${this.selected.length} ${this.valueName} selected`;
            this.current = this.multipleValue;
        },
        selectAll(){
            if (this.selected.length === this.values.length) {
                this.values.map(el => {
                    el.selected = false;
                });
                this.selected = [];
            } else if (!this.selected.length || this.selected.length > 0) {
                this.values.map(el => {
                    el.selected = true;
                });
                this.selected = this.values;
            }

            this.createMultipleValue();
        },
        closeDropOnDocumentClick(e){
            if (!this.$el.contains(e.target)) this.isShownDrop = false;
        }
    },
    mounted() {
        this.setFirstValue();
        document.addEventListener('click', this.closeDropOnDocumentClick);
    },
    destroyed () {
        document.removeEventListener('click', this.closeDropOnDocumentClick);
    },
}