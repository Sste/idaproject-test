import {getProducts, deleteProducts} from '../../assets/scripts/request';

import SelectField from '../SelectField/SelectField';
import PaginationField from '../Pagination/PaginationField';
import SortField from '../SortField/SortField';
import Ckeckbox from '../Checkbox/Checkbox';
import Confirm from '../Confirm/Confirm';

export default {
    name: 'ProductsTable',
    components: {
        SelectField,
        PaginationField,
        SortField,
        Ckeckbox,
        Confirm
    },
    data() {
        return {
            limit: 10,
            page: 1,
            currentSort: this.setDefaultSort(),
            perPageData: [
                {
                    name: '10 per page',
                    value: 10
                },
                {
                    name: '15 per page',
                    value: 15
                },
                {
                    name: '20 per page',
                    value: 20
                },
                {
                    name: '100 per page',
                    value: 100
                },
            ],
            columns: [
                {
                    name: 'Dessert (100g servin)',
                    value: 'product',
                    selected: true,
                    isSorted: false,
                },
                {
                    name: 'Calories',
                    value: 'calories',
                    selected: true,
                    isSorted: false,
                },
                {
                    name: 'Fat (g)',
                    value: 'fat',
                    selected: true,
                    isSorted: false,
                },
                {
                    name: 'Carbs (g)',
                    value: 'carbs',
                    selected: true,
                    isSorted: false,
                },
                {
                    name: 'Protein (g)',
                    value: 'protein',
                    selected: true,
                    isSorted: false,
                },
                {
                    name: 'Iron (%)',
                    value: 'iron',
                    selected: true,
                    isSorted: false,
                }
            ],
            model: {
                checkedProducts: [],
                checkedAll: false
            },
            isLoading: true,
            deletingId: null,
            confirmId: null,
            confirmMultiple: false,
        }
    },
    watch: {
        'model.checkedProducts'() {
            this.model.checkedAll = this.model.checkedProducts.length === this.products.length;
        }
    },
    computed: {
        currentPageMax(){
            return (this.limit * this.page < this.allProducts) ? this.limit * this.page : this.allProducts;
        },

        currentPageMin(){
            return (this.page - 1) * this.limit;
        },

        currenPageRange(){
            return `${this.currentPageMin + 1}-${this.currentPageMax}`
        },

        disablePagination(){
            return {
                prev: this.page <= 1,
                next: this.page >= Math.ceil(this.allProducts/this.limit)
            }
        },

        products(){
            if (this.page > Math.ceil(this.allProducts / this.limit)) {
                this.page -= 1;
            }
            let arr = this.$store.getters.getProducts;

            if (this.limit && this.page) arr = arr.slice(this.currentPageMin, this.currentPageMax);

            return arr;
        },

        allProducts(){
            const products = this.$store.getters.getProducts;
            return (products && products.length) ? products.length : 0;
        }
    },
    methods: {
        setProducts() {
            this.isLoading = true;

            getProducts().then(data => {
                this.$store.commit('SET_PRODUCTS', data);

                this.isLoading = false;
                this.$emit('onSuccess');
            }).catch(error=>{
                console.log(error);

                this.isLoading = false;
                this.$emit('onError', error);
            });
        },

        changeLimit(el){
            this.limit = el.value;
            if (this.page > this.allProducts / this.limit){
                this.page =  Math.ceil(this.allProducts / this.limit);
            }
            this.clearChecked();
        },

        changeColumnsVisability(el){
            this.columns.forEach(column => {
                if (el.value === column.value) column.selected = el.selected;
            });
            this.currentSort = this.setDefaultSort();
            this.clearChecked();
            this.$store.dispatch('sortProducts', this.currentSort);
        },

        setPrevPage(){
            if (this.page > 1) {
                this.page -=1;
                this.clearChecked();
            }
        },

        setNextPage(){
            if (this.page < Math.ceil(this.allProducts/this.limit)) {
                this.page +=1;
                this.clearChecked();
            }
        },

        setSort(cell){
            if (this.currentSort && cell.value === this.currentSort.value){
                switch (this.currentSort.dir) {
                    case 'asc':
                        this.currentSort.dir = 'desc';
                    break;
                    case 'desc':
                        this.currentSort = this.setDefaultSort();
                    break;
                }
            } else {
                this.currentSort.value = cell.value;
                this.currentSort.dir = 'asc';
            }

            this.clearChecked();

            this.$store.dispatch('sortProducts', this.currentSort);
        },

        setDefaultSort(){
            return {
                value: 'id',
                dir: 'asc'
            };
        },

        checkAll(){
            if (this.model.checkedProducts.length === this.products.length) {
                this.clearChecked();
            } else {
                this.clearChecked();
                this.products.forEach(el => {
                    this.model.checkedProducts.push(el.id);
                });
            }
        },

        clearChecked(){
            this.model.checkedProducts = [];
        },

        //TODO - add modal
        delProduct(product){
            if (this.deletingId) return false;

            this.deletingId = product.id;
            this.confirmId = product.id;

            deleteProducts().then(data => {
                console.log(data);
                this.$store.dispatch('deleteProduct', product);
                this.deletingId = null;
                alert('Item successfully deleted');
            }).catch(error=>{
                console.log(error);
                alert('Something went wrong, try again')
                this.deletingId = null;
                this.confirmId = null;
            });
        },
        //TODO - add modal
        deleteMultipleProducts(){
            this.deletingId = null;
            this.confirmId = null;

            deleteProducts().then(data => {
                console.log(data);
                this.model.checkedProducts.forEach(id => {
                    this.$store.dispatch('deleteProduct', id);
                });

                this.clearChecked();
                this.confirmMultiple = false;

                alert('Items successfully deleted');
            }).catch(error=>{
                console.log(error);
                alert('Something went wrong, try again');
            });
        },
        ascMultiple(){
            this.deletingId = null;
            this.confirmId = null;
            this.confirmMultiple = !this.confirmMultiple;
        },
        ascSingle(id){
            this.confirmId = id;
            this.confirmMultiple = false;
        },

        closeWindowOnDocumentClick(e){
            if (this.$refs.confirm && !this.$refs.confirm.contains(e.target)) this.confirmMultiple = false;
        }
    },
    mounted() {
        this.setProducts();
        document.addEventListener('click', this.closeWindowOnDocumentClick);
    },
    destroyed () {
        document.removeEventListener('click', this.closeWindowOnDocumentClick);
    },
};