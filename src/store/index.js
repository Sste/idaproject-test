import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        products: []
    },

    mutations: {
        SET_PRODUCTS (state, products) {
            state.products = products;
        },
    },

    getters: {
        getProducts(state) {
            return state.products;
        },
    },

    actions: {
        sortProducts({dispatch, commit}, data){
            let products = this.getters.getProducts,
                compare = (a, b) => {
                    return (a[data.value] > b[data.value]) ? 1 : -1;
                };

            products = products.sort(compare);

            if (data.dir === 'asc'){
                commit('SET_PRODUCTS', products);
            } else if (data.dir === 'desc'){
                commit('SET_PRODUCTS', products.reverse());
            }
        },
        deleteProduct({dispatch, commit}, data){
            let products = this.getters.getProducts;

            products.filter((n, i) => {
                let id = (data.id) ? data.id : data;
                if (n.id === id && i > -1) products.splice(i, 1);
            });

            commit('SET_PRODUCTS', products);
        }
    },

    modules: {},
});
